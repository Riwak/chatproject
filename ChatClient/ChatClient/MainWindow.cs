﻿using System;
using Gtk;
using System.Net.Sockets;
using Threading = System.Threading;
using GLib;

public partial class MainWindow : Gtk.Window
{
	// Init des Sockets et variables usuelles
	System.Net.Sockets.TcpClient clientSocket = new System.Net.Sockets.TcpClient();
	NetworkStream serverStream = default(NetworkStream);
	string readData = null;

	public MainWindow() : base(Gtk.WindowType.Toplevel)
	{
		// Construction de la fenêtre
		Build();
	}

	protected void OnDeleteEvent(object sender, DeleteEventArgs a)
	{
		// Que ce passe t'il quand je ferme la fenêtre
		Application.Quit();
		a.RetVal = true;
		serverStream.Flush();
	}

	protected void OnButton1Clicked(object sender, EventArgs e) // Connect to server button.
	{
		readData = "Conected to Chat Server ...";
		msg(); // Ajout d'un message à l'interface, les messages sont géré en pipeline par OnIdleCreate
		clientSocket.Connect("127.0.0.1", 8888); // Connection au socket serveur throught 8888
		serverStream = clientSocket.GetStream(); 

		byte[] outStream = System.Text.Encoding.ASCII.GetBytes(entry1.Text + "$"); // Envois au serveur de l'entry1.text
		serverStream.Write(outStream, 0, outStream.Length); // Sender
		serverStream.Flush(); // Clean du stream

		Threading.Thread ctThread = new Threading.Thread(getMessage); // Maintenant que je suis connecté
		ctThread.Start(); // Je lance mon thread de discussion serveur

		button1.Label = "Disconnect";
	}

	protected void OnButton2Clicked(object sender, EventArgs e) // Send request.
	{
		byte[] outStream = System.Text.Encoding.ASCII.GetBytes(entry2.Text + "$"); // Envois au serveur de l'entry2.text
		serverStream.Write(outStream, 0, outStream.Length); // Sender
		serverStream.Flush(); // Clean du stream

		entry2.Text = ""; // Remise à zéro du champs de saisi
	}

	private void getMessage() // J'écoute en même temps le serveur, #EST #ce #que #l'on #me #parle
	{
		while (true) // J'écoute tout le temps évidement
		{
			serverStream = clientSocket.GetStream(); // Ouverture du stream
			int buffSize = 0;
			byte[] inStream = new byte[clientSocket.ReceiveBufferSize]; // Init taille
			buffSize = clientSocket.ReceiveBufferSize;
			serverStream.Read(inStream, 0, buffSize); // Lecture du message
			string returndata = System.Text.Encoding.ASCII.GetString(inStream); // encodage et affichage
			readData = "" + returndata;
			msg(); // Appel de la fonction d'affichage (par pipeline FiFo)
		}
	}

	void msg()
	{
		GLib.Idle.Add(new IdleHandler(OnIdleCreate)); // Gestion First In First Out
	}

	bool OnIdleCreate()
	{
		textview1.Buffer.Text = textview1.Buffer.Text + Environment.NewLine + " > " + readData;
		return false; // True option do a infinte loop
		// True if the idle handler must be called
	}
}
