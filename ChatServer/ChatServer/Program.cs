﻿using System;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using System.Collections;

namespace ChatServer
{
	class MainClass
	{
		// La liste des clients est stocké en Hashtable
		// Association clé-élément.
		public static Hashtable clientsList = new Hashtable();

		static void Main(string[] args)
		{
			// Ecoute du port 8888, en attente de connexion client.
			TcpListener serverSocket = new TcpListener(8888);
			TcpClient clientSocket = default(TcpClient);

			// Lancement du socket d'écoute
			serverSocket.Start();
			Console.WriteLine("Chat V1 : Server Started ....");

			while ((true))
			{
				clientSocket = serverSocket.AcceptTcpClient(); // ServeurSocket/ClientSocket

				byte[] bytesFrom = new byte[clientSocket.ReceiveBufferSize]; // Récupération taille du buffer client
				string dataFromClient = null; // Empty String to manage data from client

				// Ouverture d'un stream sur le socket client 
				// Et récupération des données encodés pour la variable dataFromClient
				NetworkStream networkStream = clientSocket.GetStream();
				networkStream.Read(bytesFrom, 0, (int)clientSocket.ReceiveBufferSize);
				dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
				dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));

				// Ajout du client à la liste de broadcast
				clientsList.Add(dataFromClient, clientSocket);

				// Envois un message à tout les client connecté
				broadcast(dataFromClient + " Joined ", dataFromClient, false);


				Console.WriteLine(dataFromClient + " Joined chat room ");
				handleClinet client = new handleClinet(); // Création d'un thread de communication clientA /serveur
				// dochat() is handling the communication between the server side client into this thread
				client.startClient(clientSocket, dataFromClient, clientsList);
			}

			//Clean Socket
			clientSocket.Close();
			serverSocket.Stop();
			Console.WriteLine("exit");
			Console.ReadLine();
		}

		// Tout les client connecté recoive le message en broadcast
		// MSG = COntent
		// uName = UserName
		// flag = false by default (Qui parle en gros)
		public static void broadcast(string msg, string uName, bool flag)
		{
			// Pour chaque entrée de la liste client
			foreach (DictionaryEntry Item in clientsList)
			{
				// init le socket de broadcast
				TcpClient broadcastSocket;
				// Affectation du client
				broadcastSocket = (TcpClient)Item.Value;

				// Ouverture du stream de discussion
				NetworkStream broadcastStream = broadcastSocket.GetStream();
				Byte[] broadcastBytes = null; // Taille

				if (flag == true) // Je parle 
				{
					broadcastBytes = Encoding.ASCII.GetBytes(uName + " says : " + msg);
				}
				else // J'écoute
				{
					broadcastBytes = Encoding.ASCII.GetBytes(msg);
				}

				// On balance le message
				broadcastStream.Write(broadcastBytes, 0, broadcastBytes.Length);
				broadcastStream.Flush(); // Byebye#jeClean
			}
		}  
	}

	// Communication Client/ Serveur 
	// Chaque client à un thread de discussion séparé avec le serveur
	public class handleClinet
	{
		TcpClient clientSocket; // ref socket client
		string clNo; // Data du client 
		Hashtable clientsList; // La liste Hashtable

		public void startClient(TcpClient inClientSocket, string clineNo, Hashtable cList)
		{
			// On cp les infos pour la création du nouveau thread de discussion
			// Init Init Init 
			this.clientSocket = inClientSocket;
			this.clNo = clineNo;
			this.clientsList = cList;
			Thread ctThread = new Thread(doChat);
			ctThread.Start(); // Et c'est parti !! 
		}

		private void doChat() // Est ce que l'on me parle. lancement dans le startClient
		{
			int requestCount = 0;
			byte[] bytesFrom = new byte[clientSocket.ReceiveBufferSize]; // récupération de la taille
			string dataFromClient = null; // Init des variables
			Byte[] sendBytes = null;
			string serverResponse = null;
			string rCount = null;

			while ((true)) // J'écoute non stop dès que le client est connecté
			{
				try
				{
					requestCount = requestCount + 1;

					// Ouverture du stream de discussion
					NetworkStream networkStream = clientSocket.GetStream();
					networkStream.Read(bytesFrom, 0, (int)clientSocket.ReceiveBufferSize); // Lecture
					dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom); // Encodage
					dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$")); // récupération
					Console.WriteLine("From client - " + clNo + " : " + dataFromClient); // LogsServer
					rCount = Convert.ToString(requestCount); 

					MainClass.broadcast(dataFromClient, clNo, true); // Maintenant que j'ai rcup le msg je le broadcast
					// Msg, UserName, flag True, c'est ce client qui parle
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}
		}
	} 
}